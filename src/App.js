import React, { useState } from "react";
import List from "./List.js";
import data from "./data";

function App() {
  const [people, setPeople] = useState(data);
  return (
    <main>
      <section className="container">
        <h4>{people.length} Birthday Today</h4>
        <List data={people} />
        <button onClick={() => setPeople([])}>Clear all</button>
        <div className="description">
          <p>
            it's a Perfect example for beginner to understand the useState and
            Props in React js
          </p>
        </div>
      </section>
    </main>
  );
}

export default App;
