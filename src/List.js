import React from "react";

function List({ data }) {
  return (
    <div>
      {data.map((person) => {
        // const { id, name, age, image } = person;   we can destructure it like this
        return (
          <article key={person.id} className="person">
            <img src={person.image} alt={person.name} />
            <div>
              <h4>{person.name}</h4>
              <p>{person.age}</p>
            </div>
          </article>
        );
      })}
    </div>
  );
}

export default List;
